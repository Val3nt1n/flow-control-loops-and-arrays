package Ex4;

import java.util.Scanner;

// Write an application that for any entered number between 0 and 9 will provide it’s name. For
//example for “3” program should print “three”.

public class Ex4 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Introdu o valoare : ");
        int a = keyboard.nextInt();
        switch (a) {
            case 1:
                System.out.println("Unu");
                break;
            case 2:
                System.out.println("Doi");
                break;
            case 3:
                System.out.println("Trei");
                break;
            case 4:
                System.out.println("Patru");
            case 5:
                System.out.println("Cinci");
                break;
            case 6:
                System.out.println("Sase");
                break;
            case 7:
                System.out.println("Sapte");
                break;
            case 8:
                System.out.println("Opt");
                break;
            case 9:
                System.out.println("Noua");
                break;
            default:
                System.out.println("Nu exista!");
        }

    }
}
