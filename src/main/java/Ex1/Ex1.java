package Ex1;

import java.util.Scanner;

//Write an application that will show if entered value is greater, equal or lower than 30.

public class Ex1 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Scrie o varsta : ");
        int a = keyboard.nextInt();
        if (a < 30) {
            System.out.println("Esti adolescent!");
        }
        if (a == 30) {
            System.out.println("Esti tanar!");
        }
        if (a > 30) {
            System.out.println("Esti adult!");
        }
    }
}
