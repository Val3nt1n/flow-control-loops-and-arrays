package Ex9;


import java.util.Random;
import java.util.Scanner;

//Write an application that will find biggest value within array of int variables.

// check your application using randomly generated array (use Random class),
// check your application at least 5 times in a loop (generate random array -> print
// array to the console -> find biggest value -> print biggest value -> manually verify results).
public class Ex9 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Cate elemente doresti in array?");
        String user = keyboard.next();

        if (!user.matches("[0-9]+")) {
            System.err.println("Number invalid!");
        } else {

            int arraySize = Integer.parseInt(user);
            int[] myArray = new int[arraySize];
            Random r = new Random();

            for (int i = 0; i < myArray.length; ++i) {
                myArray[i] = r.nextInt();
            }

            System.out.print("Array generat: [ ");
            for (Integer arrayElement : myArray) {
                System.out.print(arrayElement + " ");
            }
            System.out.println(']');


            int max = Integer.MIN_VALUE;
            for (Integer arrayElement : myArray) {
                if (arrayElement > max) {
                    max = arrayElement; // when we find a bigger number in the array, that becomes the new maximum value
                }
            }
            System.out.println("\nMaximum number in the array is: " + max);

        }


    }
}