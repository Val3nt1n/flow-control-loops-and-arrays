package Ex3;

import java.util.Scanner;

//As above but only one of the values has to be greater, equal or lower than 30.

public class Ex3 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int a = keyboard.nextInt();
        int b = keyboard.nextInt();
        if (a < 30 || b > 30) {
            System.out.println("Valoarea este : " + a);
        } else if (a == 30 || b >= 30) {
            System.out.println("Valoarea este : " + b);
        } else if (a > 30) {
            System.out.println(a);
        }
    }
}
