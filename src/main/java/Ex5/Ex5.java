package Ex5;

import java.util.Scanner;

//Using nested for loops draw (parents loop iterator should be called “row”,
//child – “column”):
public class Ex5 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Input number of rows: ");
        String textNumberOfRows = keyboard.nextLine();
        if (!textNumberOfRows.matches("[0-9]+")) {
            System.err.println("Invalid input for the number of rows and columns");
            System.exit(1);
        }

        int n = Integer.parseInt(textNumberOfRows);


        rectangleWithDiagonals(n);
    }

    private static void rectangleWithDiagonals(int n) {
        for (int row = 0; row < n; row++) {
            for (int column = 0; column < n; column++) {
                if (row == 0
                        || column == 0
                        || row == n - 1
                        || column == n - 1
                        || row == column
                        || column == n - 1 - row
                ) {
                    System.out.print('*');
                } else {
                    System.out.print(' ');
                }
            }
            System.out.println();
        }
    }
}
