package Ex10;

/*
Write an application that will find the longest char sequence within an array of type String.
Test it in the same way as you have done in the previous exercise.
How will you generate random char sequences?
*/


import java.util.Random;
import java.util.Scanner;

public class Ex10 {

    private static final String POOL_OF_VALID_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz0123456789~!@#$%^&*()_+`-=";
    private static final Random randomnessSource = new Random();

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("How many elements do you want in the array?");
        String userInput = keyboard.next();

        if (!userInput.matches("[0-9]+")) {
            System.err.println("Invalid number!");

        } else {

            int arrayOfStringsSize = Integer.parseInt(userInput);
            String[] arrayOfStrings = new String[arrayOfStringsSize];

            for (int i = 0; i < arrayOfStrings.length; i++) {
                arrayOfStrings[i] = generateRandomString(randomnessSource.nextInt(100)); // the generated strings will have at most 100 chars
            }

            System.out.println("Here is your randomly generated array of strings:\n");
            for (int i = 0; i < arrayOfStrings.length; i++) {
                System.out.println("Position " + i + " -> " + arrayOfStrings[i]);
            }

            String longest_string = ""; // the shortest string is the one that has no characters
            for (String str : arrayOfStrings) {
                if (str.length() > longest_string.length()) {
                    longest_string = str;
                }
            }

            System.out.printf("\n\nThe longest string is \"%s\", having %d characters.\n\n", longest_string, longest_string.length());
        }
    }

    private static String generateRandomString(int stringLength) {
        StringBuilder charAccumulator = new StringBuilder(stringLength);

        for (int i = 0; i < stringLength; i++) {

            // get a random position from the pool of valid characters
            int randomPosition = randomnessSource.nextInt(POOL_OF_VALID_CHARS.length());

            // get the character at that position
            char characterAtRandomPosition = POOL_OF_VALID_CHARS.charAt(randomPosition);

            // add the character to our resulting string
            charAccumulator.append(characterAtRandomPosition);
        }

        return charAccumulator.toString();
    }

}
