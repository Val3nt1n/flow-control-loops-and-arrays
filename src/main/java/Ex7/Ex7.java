package Ex7;
//Write a “divide by” application.
// User should be able to enter initial value that will be divided in a loop by a new value entered by a user.
// Division should occur as long, as entered value will be different than 0.
// Result of division should be rounded to the fourth decimal point and printed to the console.

import java.util.Scanner;

public class Ex7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String numarator = scanner.next();
        String numitor = scanner.next();
        if (numarator.matches("[0-9]+") && numitor.matches("[0-9]+")) {
            float numaratorFloat = Float.parseFloat(numarator);
            float numitorFloat = Float.parseFloat(numitor);
            while (numitorFloat != 0) {
                float impartire = numaratorFloat / numitorFloat;
                System.out.printf("%.4f ", impartire);
                numitorFloat = scanner.nextInt();
            }
        }
        System.err.println("Invalid input");
    }
}
