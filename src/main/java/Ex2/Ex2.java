package Ex2;

import java.util.Scanner;

//As above but compare two values at the same time. Verify if first value is greater than 30 and
//second value is greater than 30, and so on.
public class Ex2 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Introduceti primul numar : ");
        int a = keyboard.nextInt();
        System.out.print("Introduceti al-2-lea numar : ");
        int b = keyboard.nextInt();
        if (a < 30 && b < 30) {
            System.out.println("Valoarea este mai mica decat 30 ");
        } else if (a > 30 && b > 30) {
            System.out.println("Valoarea este mai mare decat 30 ");
        } else if (a == 30 && b == 30) {
            System.out.println("Valoarea este egala cu 30 ");
        } else {
            System.out.println("Este alta valoare!");
        }
    }
}
