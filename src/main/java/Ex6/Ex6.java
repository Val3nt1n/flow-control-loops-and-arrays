package Ex6;

import java.util.Scanner;

public class Ex6 {
    public static void main(String[] args) {
        //  milk();
        // milkCorect();
        Wine();
    }

    private static void Wine() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Are you and adult? Respond with Y/N");
        String answer = scanner.next();
        if (answer.matches("N|n")) {
            System.out.println("You are not allowed to buy alcohol");

        } else if (answer.matches("Y|y")) {
            float pretProdus = 112.35f;
            System.out.printf("Please give me %.2f  lei\n", pretProdus);
            String userInput = scanner.next();
            if (userInput.matches("[0-9]+\\.[0-9]+")) {
                float payment = Float.parseFloat(userInput);
                if (payment == pretProdus) {
                    System.out.println("Thank you, here is the receipt");
                } else if (payment > pretProdus) {
                    float rest = payment - pretProdus;
                    System.out.printf("Here is %.2f lei change", rest);
                } else if (payment < pretProdus) {
                    float diferenta = pretProdus - payment;
                    System.out.printf("I need the following ammount to complete de transaction :  %.2f lei", diferenta);
                }
            }
        } else {
            System.err.println("Input is invalid");
        }
    }


    private static void milkCorect() {
        Scanner scanner = new Scanner(System.in);
        float pretProdus = 5.75f;
        System.out.printf("Please give me %.2f  lei\n", pretProdus);
        String userInput = scanner.next();
        if (userInput.matches("[0-9]+\\.[0-9]+")) {
            float payment = Float.parseFloat(userInput);
            if (payment == pretProdus) {
                System.out.println("Thank you, here is the receipt");
            } else if (payment > pretProdus) {
                float rest = payment - pretProdus;
                System.out.printf("Here is %.2f lei change", rest);
            } else if (payment < pretProdus) {
                float diferenta = pretProdus - payment;
                System.out.printf("I need the following ammount to complete de transaction :  %.2f lei", diferenta);
            }
        } else {
            System.err.println("Input is invalid");
        }

    }


    private static void milk() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("What would you like to buy?");
        String product = scanner.next();
        if (product.matches("[0-9]+\\.[0-9]+"))
            if (product.matches("milk")) {
                System.out.printf("The milk costs %.2f", 5.50);
                System.out.println("\nPlease enter the required ammount:");
            }
        double plata = scanner.nextDouble();
        if (plata == 5.50) {
            System.out.println("Thankyou, here is the receipt");
        } else if (plata > 5.50) {
            System.out.println("Here is the rest : " + (plata - 5.50) + "lei and your receipt");
        } else if (plata < 5.50) {
            System.out.println("I need the following ammount to complete de transaction : " + (5.50 - plata) + " lei");
        } else {
            System.err.println("Input is invalid");
        }
    }
}


