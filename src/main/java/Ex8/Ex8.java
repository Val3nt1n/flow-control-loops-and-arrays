package Ex8;

import java.util.Scanner;

//Write a simple “echo” application, that will:
//
//print back entered string,
//go to the beginning of a loop if user will enter “continue”,
//break the loop with a “good bye!” message, if user will enter “quit”.
public class Ex8 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        while (true) {
            System.out.println("\nWrite something: 'Quit / quit / q for exit' ");
            final String line = keyboard.nextLine();
            if (line.toUpperCase().matches("CONTINUE")) {
                continue;
            }
            if (line.toUpperCase().matches("QUIT|Q")) {
                System.out.println("Good Bye");
                break;
            }
            System.out.print(line);
        }
    }
}